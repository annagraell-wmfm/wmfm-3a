# !/usr/bin/python3
# -*- coding: utf-8 -*-

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
from toolsR import VideoR as vr


"""
TEST VIDEO: it is the same of videoTest, in addition you have the flag tracking to True so 
when you record you have a window to select the boxes. 
"""
def start():
    """
    nAreas: is the number of boxes for the tracking
    address: bpod address
    tracking = flag to use the tracking
    """
    video = vr(0, path="/home/parallels/Videos/", fps=60, tracking=True, nAreas=3, bpod_address=36000) # <------ create the folder video
    video.play()
    video.record()
    time.sleep(30)
    video.stop()
    return


if __name__ == '__main__':
    start()
