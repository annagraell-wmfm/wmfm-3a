# !/usr/bin/python3
# -*- coding: utf-8 -*-

from multiprocessing import Process
import sounddevice as sd, numpy as np


class SoundR():
    def __init__(self, sampleRate = 44100, deviceOut = 'default', channelsOut = 2, latency='low'):
        sd.default.device = deviceOut
        sd.default.samplerate = sampleRate
        # sd.default.blocksize = 384
        sd.default.latency = latency
        sd.default.channels = channelsOut

        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.close()
        self._sound = []

    @staticmethod
    def getDevices():
        return sd.query_devices()

    def load(self, v1, v2=None):
        """ Load audio """
        if v2 is None:
            v2 = v1
        if len(v1) != len(v2):
            raise ValueError('SoundR: The length of the vectors v1 and v2 has be the same.')
        try:
            if self._p.is_alive():
                print('SoundR: It is not possible to load. The process is running. Wait! ')
                return
        except AttributeError:
            pass
        sound = self._createSoundVec(v1,v2)
        self._Stream.close()
        self._Stream = sd.OutputStream(dtype='float32')
        self._Stream.start()
        self._sound = sound
        self._p = Process(target=self._playSoundBackground)

    def playSound(self):
        if self._sound == []:
            raise ValueError('SoundR: No sound loaded. Please, use the method load().')
        if self._p.is_alive():
            print('SoundR: It is not possible to play. The process is running. Wait!')
            return
        try:
            self._p.start()
        except AttributeError:
            print('SoundR: The process is not present. Recall the function to fix the problem.')
        except AssertionError:
            print('SoundR: Load the sound again before to use playSound().')


    def stopSound(self):
        try:
            if self._p.is_alive():
                #self.Stream.abort()
                self._Stream.close()
                self._p.terminate()
                print('SoundR: Stop.')
            else:
                print('SoundR: it is not possible to stop. No process is running.')
        except AttributeError:
            print('SoundR: it is not possible to stop. No process is running.')

    def _playSoundBackground(self):
        print('SoundR: Play.')
        if self._sound == []:
            print('Error: no sound is loaded.')
        else:
            self._Stream.write(self._sound)

    def _createSoundVec(self, v1, v2):
        sound = np.array([v1, v2])  # left and right channel
        return np.ascontiguousarray(sound.T, dtype=np.float32)


