from threading import Thread, Event
import evdev
from evdev import categorize
import numpy as np
from select import select

class Recorder(Thread):
    
    def __init__(self, queue):
        print("Well, I'm initializing recorder now...")
        Thread.__init__(self)
        self.queue = queue
        # Events used to sync with main control.py:
        self.record = Event()
        self.get_answer = Event()
        self.answer_sent = Event()
        self.send_log = Event()
        self.stop = Event()
        self.log_sent = Event()
        # Vars to hold data:
        self.data_buffer = None
        self.trial_answer = None

    def run(self):
        print("I'm running run!!")
        while True:
            self.data_buffer = []  # Clear the buffer.
            self.trial_answer = None  # Clear the answer.
            self.record.wait()
            while self.record.is_set(): #while record is set, get_coords 
                self.get_coords()
            if self.stop.is_set():
                self.stop.clear()
                break  # Poison pill, exit and finish the thread.

    def close(self):
        pass

    def get_coords(self):
        
        # method to get coords goes here, and updates self.data_buffer
        i = 0
        j = 0
        a = []
        coord = 0
        c = np.zeros([1,3])
        #c = []
        device = evdev.InputDevice('/dev/input/by-id/usb-6615_0001-event-if00') #find event number when newly conected!
        ready, _, _ = select([device], [], [], 180)

        print("I'm trying to get the coords")
    
        if ready:
            print("I'm inside ready")
            for event in device.read_loop():
                print("I'm reading events!")
                print(self.get_answer.is_set())
                if event.type == evdev.ecodes.EV_ABS:       #if event is a coordinate
                    print(event)
                    coord = 'ABS_X' if event.code == 0 else 'ABS_Y'         #add label x or y
                    a.append([event.value, coord, event.timestamp()])       #a=[coord, label, timestamp] #,event.sec,event.usec])
                
                if event.type == evdev.ecodes.EV_KEY and event.value != 1:  #if BTN_TOUCH up
                    b=np.zeros([1,3])                       #create array to store pairs of coord
                    print(f"c is:{c}")
                    print(f"b trying to concat is:{b}")
                    if not a:
                        b = np.concatenate((b,c),axis=0)
                           
                    if len(a) == 1:
                        #b[0] = c[0]
                        b = np.concatenate((b,c),axis=0)
                        if 'ABS_X' in a[j]:                             #record the coord in a new b row
                            newrow = np.array([[a[j][0],np.nan,a[j][2]]])       #and write nan to missing pair
                            b = np.concatenate((b,newrow),axis=0)
                        if 'ABS_Y' in a[j]:
                            newrow =  np.array([[np.nan,a[j][0],a[j][2]]])
                            b = np.concatenate((b,newrow),axis=0) 
                    
                    while j<len(a)-1:
                        #Pairing of the coordinates into x,y

                        if a[j][2] == a[j+1][2] and a[j][1] != a[j+1][1]:                       #if same time, combine coord
                            newrow = np.array([[a[j][0],a[j+1][0],a[j][2]]],dtype=float)        #also check if they are x and y (not x,x or y,y)
                            b = np.concatenate((b,newrow),axis=0)           #add pair to b array
                            i = i+1
                            j = j+2 #in pairs

                        else:                                               #if time is different
                            if 'ABS_X' in a[j]:                             #record the coord in a new b row
                                newrow = np.array([[a[j][0],np.nan,a[j][2]]])       #and write nan to missing pair
                                b = np.concatenate((b,newrow),axis=0)
                            if 'ABS_Y' in a[j]:
                                newrow =  np.array([[np.nan,a[j][0],a[j][2]]])
                                b = np.concatenate((b,newrow),axis=0)
                            i = i+1
                            j=j+1
                        
                    for row_idx in range(b.shape[0]):                       
                        for col_idx in range(b.shape[1]):                   
                            if np.isnan(b[row_idx][col_idx]):               #replace nan with missing pair
                                b[row_idx][col_idx] = b[row_idx-1][col_idx] #taking the last recorded coord of the missing type
                    
                    b=np.delete(b,(0),axis=0)       #remove first row in b, which is by default [0,0,0]
                                                    #b contains all coords between BTN_down and _up, as numpy.float64
                    c = [b[len(b)-1]]
                    a.clear()                                               #start recording new coord
                    j=0
                    
                    self.data_buffer.append(b.tolist())
                    
                    if self.get_answer.is_set() == True: #
                        break #
                    #if not self.record.is_set(): #I added this 6/05/2019
                    #    break
        print("I'm before answer is set")
        if self.get_answer.is_set():    # AND buffer has been updated
            print("get_answer was set")
            self.get_answer.clear()     # deactivate flag
            # update trial_answer with the first????? answer
            if not self.data_buffer:
                self.trial_answer = self.data_buffer
            else:
                self.trial_answer = self.data_buffer[0] #[0]
            print(self.trial_answer)
            self.answer_sent.set()
            
            
        
