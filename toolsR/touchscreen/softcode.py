# !/usr/bin/python3
# -*- coding: utf-8 -*-

import socket

class SoftcodeConnection:

    def __init__(self):
        """ Open the connection """
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.client_socket.settimeout(1.0)
        self.address = ("127.0.0.1", 36000)

    def send(self, idx):
        """ Send the softcode to the port idx """
        str_message = "SoftCode"+str(idx)
        message = str_message.encode('utf-8')
        self.client_socket.sendto(message, self.address)
        self.client_socket.sendto(message, self.address)
        stopMessage = b's'
        self.client_socket.sendto(stopMessage, self.address)

    def close(self):
        """ Close the connection """
        self.client_socket.close()
