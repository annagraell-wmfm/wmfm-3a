import os
import logging
import datetime
from queue import Queue

import psychopy
from psychopy import visual, core, event
import math
import numpy
from numpy import linalg as ln
from psychopy import event, visual

from toolsR.touchscreen.recorder import Recorder
from toolsR.touchscreen.softcode import SoftcodeConnection

SOFTC_REWARD = 10
SOFTC_PUNISH = 12

class TouchscreenR:
    """
    Docs.
    """
    def __init__(self, threshold, win): #posar variables aquí si venen del pybpod (útil per les q es defineixen un sol cop per sessió), i definir-les també com self.variable = variable a sota
        print("I am initializing TouchscreenR")
        self.stimulus = None
        #self.stimulus = 200
        #self.stimduration = stimduration
        self.threshold = threshold #threshold to define correct or incorrect, norm (in pixels = 0.28mm2)

        self.last_answer = None
        self.queue = Queue()

        self.recorder = Recorder(self.queue)
        self.recorder.start()

        self.softcode = SoftcodeConnection() #initialize SoftcodeConnection since we will be calling its functions here
        
        # self.win = visual.Window([1440,900],screen=1,color=[-1,-1,-1],
        # units='pix',fullscr=False,viewPos=[-720,450])    #screen 1 for second monitor #CHANGE FULLSCR = TRUE AT THE END!
        # print("I just opened the black window")
        self.win = win

    # ----------------- COMMANDS: --------------------------
    
    def start_trial(self):
        """ 
        Start recording pokes.
        """
        print("I set record")
        
        self.recorder.record.set()


    def finish_trial(self):
        """
        Stop recording pokes.
        """
        print("I arrived to finish_trial")
        self.recorder.data_buffer=[]        
        self.recorder.record.clear()

    def stop(self):
        """
        Stop thread and release resources.
        """
        self.recorder.stop.set()
        self.recorder = None

    def show_stim(self):#threshold to define correct or incorrect
        """
        Tell psychopy to show stimulus for given stimduration
        """
        print(self.stimulus)
        stim = visual.Circle(win=self.win, radius=35, edges=32,lineColor='black',
        fillColor='white',units='pix',pos=[self.stimulus,-770]) #-800
        stim.draw()
        self.win.flip()

    def hide_stim(self):
        """
        Tell psychopy to hide the stimulus.
        """
        print("I hid the stim cause utold me so")
        self.win.flip() #updates win
        self.win.flip() #updates win
        self.win.flip() #updates win

    def wait_response(self): 
        """
        Set thread event to track response.
        """
        print("I should set get_answer next")
        self.recorder.get_answer.set() 
        #self.timer = QTimer()
        #self.timer.singleShot(5, self.stop_trial)
        print("I just came back from setting get_answer")
        self.recorder.answer_sent.wait()
        print("Waitin")
        self.recorder.answer_sent.clear()
        self.last_answer = self.recorder.trial_answer #in fact it is now first answer
        xpsy = abs(self.stimulus)
        ypsy = 770 #by now

        if not self.last_answer:
            print("Did not respond, SoftCode3")
            print(f"psychopy coord were:{[xpsy,ypsy]}")
            self.softcode.send(3) #(idx) = 'SoftCodeidx' is sent
        else:
            #if len(self.last_answer)>1:
            #    i = len(self.last_answer)-1
            #else:
            #    i = 0
            i = 0
            #### convert from touch resolution [4096,4096] to psycho resolution [1440,900] ####
            xtouch = abs(self.last_answer[i][0]*(1440/4096))
            ytouch = abs(self.last_answer[i][1]*(900/4096))

            #### calculate norm from touched to psycho point
            if ln.norm(numpy.array((xtouch,ytouch))-numpy.array((xpsy,ypsy))) < self.threshold: #just to test, this should be evaluating corr or non-corr
                print("Answer was correct < threshold")
                print(f"psychopy coord were:{[xpsy,ypsy]}")
                print(f"answered coords were:{[xtouch,ytouch]}")
                #and send the softcode to bpod
                self.softcode.send(1) #(idx) = 'SoftCodeidx' is sent == Correct
            else:
                print("Answer was incorrect, send SoftCode2")
                print(f"psychopy coord were:{[xpsy,ypsy]}")
                print(f"answered coords were:{[xtouch,ytouch]}")
                self.softcode.send(2)
        

	# self.last_answer = self.queue.get()
        # Decide if the answer was correct or not
            #threshold must be interactive, to be defined in pybpod for each trial if needed
            #send 3xcorrect/incorrect Softcode (Albert says one), in case one softcode gets lost
        # and send softcode back to bpod

    def send_record_to_bpod(self):
        """
        Send the coordinates register to bpod.
        """
        #print(self.recorder.data_buffer)
        #return self.recorder.data_buffer
        print(self.last_answer)
        return self.last_answer
        
if __name__ == '__main__':
    threshold = 300 #finger error is usually 50
    #PSYCHO:
    win = visual.Window([1440,900],screen=1,color=[-1,-1,-1],units='pix',fullscr=False,viewPos=[-720,450])   #screen 1 for second monitor #CHANGE FULLSCR = TRUE AT THE END!
    screen = TouchscreenR(threshold, win)
    screen.stimulus([200, 1000, 300, 560, 439])
    print('Hi')
