from queue import Queue
from toolsR.utils import TimerR
from threading import Thread
from time import strftime, gmtime
from PIL import Image
from PIL import ImageDraw

import os
import cv2

import numpy as np

class VideoR:
    
    def __init__(self, indx_or_path=0, name_video=None, path=None, width=None, height=None, fps=None, codec_cam='MJPG',
                 codec_video='MJPG', showWindows=True, title="Streaming Window", mask=False):
           
        # Control
        if type(indx_or_path) is not int:
            raise ValueError('VideoR: The indx_or_path has to be an Integer, it is 0 as default.')
        if type(name_video) is not str and name_video is not None:
            raise ValueError('VideoR: The name_video has to be a String.')
        if type(path) is not str and path is not None:
            raise ValueError('VideoR: The path has to be a String.')
        elif path is not None:
            if not os.path.exists(path):
                raise ValueError('VideoR: The path = ' + path + ' does not exist.')
        if type(width) is not int and width is not None:
            raise ValueError('VideoR: The width has to be an Integer.')
        if type(height) is not int and height is not None:
            raise ValueError('VideoR: The height has to be an Integer.')
        if type(fps) is not int and fps is not None:
            raise ValueError('VideoR: The fps has to be an Integer.')
        if type(codec_cam) is not str:
            raise ValueError('VideoR: The codec_cam has to be a String, it is MJPG as default.')
        if type(codec_video) is not str:
            raise ValueError('VideoR: The codec_video has to be a String,  it is MJPG as default.')
        if type(showWindows) is not bool:
            raise ValueError('VideoR: The flag has to be a Boolean, it is True as default.')
            
        self._idx = indx_or_path
        self._nv = name_video
        self._pt = path
        self._wt = width
        self._hg = height
        self._fps = fps
        self._codecCamera = codec_cam
        self._codecVideo = codec_video
        self._showW = showWindows
        self._title= str(title)
        self._consumer = None
        self._mask = mask
        self.queue = Queue()
        
    def play(self):
        try:  # If it is the first time it should catch the exception and create the thread
            test = self.is_open
        except AttributeError:
            self._consumer = CameraThread(self.queue, self._idx, self._nv, self._pt, self._wt,
                                        self._hg, self._fps, self._codecCamera, self._codecVideo, self._showW, self._title, self._mask)
            self._consumer.start()
            return
        else:
            if test:
                print('VideoR: The camera is already running.')
            
    def record(self):
        try:
            test = self.is_open
        except AttributeError:
            print('VideoR: The camera does not start. Use the play() method before recording.')
        else:
            if test:
                self.queue.put('record')
            else:
                print('VideoR: The camera object was closed.')

    def pause(self):
        try:
            test = self.is_open
        except AttributeError:
            print('VideoR: The camera does not start. Use the play() method before recording.')
        else:
            if test:
                self.queue.put('pause')
            else:
                print('VideoR: The camera object was closed.')

    def stop(self):
        try:
            test = self.is_open
        except AttributeError:
            print('VideoR: The camera does not start. Use the play() method before recording.')
        else:
            if test:
                self.queue.put('stop')
                self._consumer = None
            else:
                print('VideoR: The camera object was closed.')
        
    @property
    def is_open(self):
        return self._consumer.is_alive()
            
class CameraThread(Thread):
    
    def __init__(self, queue, idx_camera, name_video, path, width, height,
                 fps, fourcc_cam, fourcc_video, showWin, title, mask):
        
        Thread.__init__(self)
        self.queue = queue
        self.video = cv2.VideoCapture(idx_camera)
        self.name_video = name_video
        self.path = path
        self.fps = []
        self.width = []
        self.height = []
        self.isSaving = False  # Is True only when starting to record.
        self.action = -1  # Will hold the next command from the queue.
        self.frame_counter = 0
        self.showW = showWin
        self.title = title
        self.mask = mask
        self.action = 'play'  # Starting action

        fourcc_cam = cv2.VideoWriter_fourcc(*fourcc_cam)
        self.video.set(cv2.CAP_PROP_FOURCC, fourcc_cam)
        if width is not None:
            self.video.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        if height is not None:
            self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        if fps is not None:
            self.video.set(cv2.CAP_PROP_FPS, fps)
        self.fps = int(self.video.get(cv2.CAP_PROP_FPS))
        self.width = int(self.video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        # Record the video
        self.fourccOut = cv2.VideoWriter_fourcc(*fourcc_video)
        if name_video is None:
            self.name_video = strftime("%d%b%Y_%H%M%S.mkv", gmtime())
        if path is None:
            self.path = ''
        self.outVideo = None # It opens a channel to save the video only when the variable isSaving=true
        self.frame = np.zeros([self.height, self.width])

    def run(self):
        chrono = TimerR()
        # fps counter:
        fps_now     = 0
        counter_fps = 0 
        last_sec    = 0 
        while True:
            # Get the command:
            if not self.queue.empty():
                self.action = self.queue.get()
            if self.action == 'play':
                if not chrono.isRunning() or chrono.isPause(): # Start the chronometer
                    chrono.start()
            # Check the command and change the variables:
            if self.action == 'record':
                if not self.isSaving:
                    self.outVideo = cv2.VideoWriter(self.path + self.name_video, 
                                                    self.fourccOut, self.fps, 
                                                    (self.width, self.height))
                    self.isSaving = True
            elif self.action == 'pause':
                chrono.pause()
            elif self.action == 'stop':
                if chrono.isRunning():  # Close the timer
                    chrono.stop()
                    chrono.reset()
                break  # Poison pill
            
            ret, frame = self.video.read()
            if ret:
                if self.action in ('record', 'play'):
                    if self.action == 'record':
                        self.outVideo.write(frame)
                    self.frame_counter += 1
                    counter_fps += 1
                    now_sec = int(chrono.getTimeSeconds())
                    if now_sec % 5 == 0 and now_sec != last_sec: # reset the fps counter each 5 sec
                        last_sec = int(now_sec)
                        fps_now = int(counter_fps / 5)
                        counter_fps = 0
                    cv2.putText(frame, 'fps: ' + str(fps_now) + ' time: ' + chrono.getString() + ' Rec',
                                (10, 30),
                                cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
                elif self.action == 'pause':
                    if chrono.isRunning():
                        cv2.putText(frame, 'fps: ' + str(fps_now)+ ' time: ' + chrono.getString() + ' Pause',
                                    (10, 30), cv2.FONT_HERSHEY_DUPLEX, 0.5, (255, 255, 255), 1, cv2.LINE_AA) 
                if self.showW:
                    if self.mask:
                        img = Image.fromarray(frame)
                        draw = ImageDraw.Draw(img)
                        draw.line([(319, 0), (319, 480)], fill=(0,0,0), width=3)
                        draw.arc((290,180,350,240), 180, 0,'black', width=2)
                        draw.ellipse((245,135,280,170), outline='black', width=2)
                        draw.ellipse((355,135,390,170), outline='black', width=2)
                        cv2.imshow("Streaming Camera+Mask", np.array(img))
                    else:
                        cv2.imshow(self.title, frame)
                    # Exit if ESC pressed
                    k = cv2.waitKey(30) & 0xff
                    if k == 27:
                        break
            else:
                raise IndexError('VideoR: Camera not found. Check the connection and the index.')
        self.close()
        return
    
    def close(self):  # Release the camera and close the windows, then save the file.
        self.video.release()
        cv2.destroyAllWindows()
        if self.outVideo is not None:
            self.outVideo.release()
            
if __name__ == '__main__':
    camera = VideoR(indx_or_path=0, width=800, height=800, title='hey',codec_cam='MJPG',
                 codec_video='MJPG',path='/home/graell/Videos/')
    camera.play()
